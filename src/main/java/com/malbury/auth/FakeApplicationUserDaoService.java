package com.malbury.auth;

import com.google.common.collect.Lists;

import com.malbury.utility.PasswordEncoder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

import static com.malbury.security.ApplicationUserRole.ADMIN;
import static com.malbury.security.ApplicationUserRole.STUDENT;

@Repository
public class FakeApplicationUserDaoService implements ApplicationUserDao{

     private final PasswordEncoder passwordEncoder;

    public FakeApplicationUserDaoService(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }


    @Override
    public Optional<ApplicationUser> selectAppUserByUsername(String userName) {
        return getApplicationUsers()
                .stream()
                .filter(applicationUser -> userName.equals(applicationUser.getUsername()))
                .findFirst();
    }


    private List<ApplicationUser> getApplicationUsers(){
        List<ApplicationUser> applicationUsers = Lists.newArrayList(
                new ApplicationUser(
                        STUDENT.getGrantedAuthorities(),
                        passwordEncoder.bCryptPasswordEncoder().encode("nurlan"),
                       "nurlan",
                        true,
                        true,
                        true,
                        true

                ),
                new ApplicationUser(
                        ADMIN.getGrantedAuthorities(),
                        passwordEncoder.bCryptPasswordEncoder().encode("linda"),
                        "linda",
                        true,
                        true,
                        true,
                        true

                )
        );
        return applicationUsers;
    }
}
