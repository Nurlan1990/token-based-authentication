package com.malbury.auth;


import java.util.Optional;

public interface ApplicationUserDao {

     Optional<ApplicationUser> selectAppUserByUsername(String userName);
}
