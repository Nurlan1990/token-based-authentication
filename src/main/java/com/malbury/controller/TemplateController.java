package com.malbury.controller;

import com.malbury.jwt.UsernameAndPasswordAuthRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/")
public class TemplateController {

    @GetMapping("login")
    public String getLogin(){
        return "login";
    }

    @GetMapping("courses")
    public String getCourses(){
        return "courses";
    }

  /*  @PostMapping("login")
    public String setUsernameAndPAssword(
            @RequestParam(name = "username") String username,
            @RequestParam(name = "password") String password){

        System.out.println("username " + username);
        System.out.println("password " + password);
        UsernameAndPasswordAuthRequest usernameAndPasswordAuthRequest = new UsernameAndPasswordAuthRequest();
        usernameAndPasswordAuthRequest.setUsername(username);
        usernameAndPasswordAuthRequest.setPassword(password);

        return "setLogin";
    }*/
}
