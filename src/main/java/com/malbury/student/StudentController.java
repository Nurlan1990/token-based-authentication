package com.malbury.student;

import com.malbury.jwt.UsernameAndPasswordAuthRequest;
import com.malbury.model.Student;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/api/v1/students")
public class StudentController {

    private static final List<Student> studentList = Arrays.asList(
            new Student(1,"James"),
            new Student(2,"Maria"),
            new Student(3,"Anna")
    );

    @GetMapping("/{studentId}")
    public Student getStudent(@PathVariable("studentId") Integer studentId){
        return studentList.stream()
         .filter(student -> studentId.equals(student.getStudentId()))
         .findFirst()
         .orElseThrow(() -> new IllegalArgumentException("Student " + studentId + " doesnt exist"));
    }



}
