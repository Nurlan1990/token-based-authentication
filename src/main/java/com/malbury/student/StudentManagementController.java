package com.malbury.student;

import com.malbury.model.Student;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/management/api/v1/students")
public class StudentManagementController {

    private static final List<Student> studentList = Arrays.asList(
            new Student(1,"James"),
            new Student(2,"Maria"),
            new Student(3,"Anna")
    );

    @GetMapping("/")
    public List<Student> getStudents(){
        return studentList;
    }

    @PostMapping("/")
    public void registerNewStudent(@RequestBody Student student){
        System.out.println(student);
    }

}










